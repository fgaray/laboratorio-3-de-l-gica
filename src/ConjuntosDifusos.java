import nrc.fuzzy.*;


/**Esta clase se encarga de inicializar los conjuntos difusos y de mentenerlos
 * en un solo lugar.
 * */
class ConjuntosDifusos{

    FuzzyVariable peso, corrosion, historia_carga, fatiga;


    /**Constructor, no recive ningun argumento*/
    ConjuntosDifusos()throws FuzzyException
    {
        this.iniciarPeso();
        this.inicarCorrocion();
        this.inicarHistoriaCarga();
        this.iniciarCiclosFatiga();

    }


    /**Incializa la variable peso.
     *
     * La variable peso tiene 4 conjuntos difusos: poco, mediano, mucho y
     * demaciado en forma de trapesoides. Esta variable esta en la unidad de
     * kilogramo.
     * */
    private void iniciarPeso()throws FuzzyException
    {
        this.peso = new FuzzyVariable("Peso en la viga", 0, 4000, "Kg");

        this.peso.addTerm("poco", new TrapezoidFuzzySet(0, 0, 600, 700));
        this.peso.addTerm("mediano", new TrapezoidFuzzySet(600, 700, 1100, 1200));
        this.peso.addTerm("mucho", new TrapezoidFuzzySet(1100, 1200, 2000, 2100));
        this.peso.addTerm("demaciado", new TrapezoidFuzzySet(2000, 2100, 4000, 4000));
    }


    /**Incializa la variable corrocion.
     * 
     * La variable corrocion se basa en que tan lejos se esta del mar. Esta
     * variable se mide en millas con respecto al mar.
     * */
    private void inicarCorrocion()throws FuzzyException
    {
        this.corrosion = new FuzzyVariable("Cercania al mar", 0, 80, "millas");
        this.corrosion.addTerm("cercano", new TrapezoidFuzzySet(0, 0, 20, 30));
        this.corrosion.addTerm("urbano", new TrapezoidFuzzySet(20, 30, 50, 60));
        this.corrosion.addTerm("rural", new TrapezoidFuzzySet(50, 60, 80, 80));
    }


    /**Incializa la variable historia de carga.
     * 
     * La historia de carga es la vida de la viga (que tan vieja es la viga). Se mide en anos.
     *
     * */
    private void inicarHistoriaCarga()throws FuzzyException
    {
        this.historia_carga = new FuzzyVariable("Historia de carga", 0, 150, "anos");
        this.historia_carga.addTerm("poco_cargada", new TrapezoidFuzzySet(0, 0, 10, 15));
        this.historia_carga.addTerm("cargada_medianamente", new TrapezoidFuzzySet(10, 15, 50, 60));
        this.historia_carga.addTerm("muy_cargada", new TrapezoidFuzzySet(50, 60, 100, 120));
        this.historia_carga.addTerm("demaciado_cargada", new TrapezoidFuzzySet(100, 120, 150, 150));
    }

    /**Inicializa la variable ciclos de fatiga.
     *
     * Las vigas experimentan vibraciones que van debilitando la estructura
     * interna. Esta variable mide este efecto en "ciclos"
     * */
    private void iniciarCiclosFatiga()throws FuzzyException
    {
        this.fatiga = new FuzzyVariable("Fatiga", 0, 10000000, "ciclos");
        this.fatiga.addTerm("ciclos_bajos", new TrapezoidFuzzySet(0, 0, 1000000, 1200000));
        this.fatiga.addTerm("ciclos_medios", new TrapezoidFuzzySet(1000000, 1200000, 3000000, 3200000));
        this.fatiga.addTerm("muchos_ciclos", new TrapezoidFuzzySet(3000000, 3200000, 5000000, 5200000));
        this.fatiga.addTerm("demaciados_ciclos", new TrapezoidFuzzySet(5000000, 5200000, 10000000, 10000000));
    }


    /**Getter de la variable peso
     *
     * @return La variable peso
     * */
    public FuzzyVariable getPeso()
    {
        return this.peso;
    }


    /**Getter de la variable corrocion
     *
     * @return La variable corrocion
     * */
    public FuzzyVariable getCercaniaMar()
    {
        return this.corrosion;
    }


    /**Getter de la variable corrocion
     *
     * @return La variable corrocion
     * */
    public FuzzyVariable getHistoriaCarga()
    {
        return this.historia_carga;
    }

    /**Getter de la variable ciclos de fatiga
     *
     * @return La variable ciclos de fatiga
     * */
    public FuzzyVariable ciclosFatiga()
    {
        return this.fatiga;
    }

}
