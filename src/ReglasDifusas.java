import nrc.fuzzy.*;
import java.util.*;


/**Esta clase crea las reglas difusas y saca inferencias logica a partir de
 * estas*/
class RegasDifusas{

    //En este arreglo se guardan las reglas difusas
    ArrayList<FuzzyRule> reglas;


    ConjuntosDifusos conjuntos;


    //Las conclusiones de la inferencia difusa
    FuzzyValue resiste;
    FuzzyValue mantenimiento;
    FuzzyValue no_resiste;




    //definimos los valores que vamos a usar para definir las reglas
    FuzzyValue peso_poco;
    FuzzyValue peso_mediano;
    FuzzyValue peso_mucho;
    FuzzyValue peso_demaciado;


    FuzzyValue cercano_mar;
    FuzzyValue urbano;
    FuzzyValue rural;


    FuzzyValue historia_poca;
    FuzzyValue historia_mediana;
    FuzzyValue historia_bastante;
    FuzzyValue historia_demaciada;



    FuzzyValue fatiga_bajo;
    FuzzyValue fatiga_medio;
    FuzzyValue fatiga_mucho;
    FuzzyValue fatiga_demaciado;


    FuzzyValue peso_normal;
    FuzzyValue no_mar;
    FuzzyValue historia_normal;
    FuzzyValue fatiga_normal;






    /**Constructor que inicializa los valores de los FuzzyValue y de las reglas.
     *
     * @param conjuntos Un objeto ConjuntosDifusos que contiene los conjuntos
     * necesarios para realizar la inferecnia logica*/
    RegasDifusas(ConjuntosDifusos conjuntos)throws FuzzyException
    {
        this.reglas = new ArrayList<FuzzyRule>();

        this.conjuntos = conjuntos;

        FuzzyRule regla;

        // Variable para la conclusion
        FuzzyVariable conclusion = new FuzzyVariable("Conclusion", 0, 100, "%");
        conclusion.addTerm("resiste", new TrapezoidFuzzySet(0, 0, 25, 33));
        conclusion.addTerm("mantenimiento", new TrapezoidFuzzySet(25, 33, 66, 75));
        conclusion.addTerm("no_resiste", new TrapezoidFuzzySet(66, 75, 100, 100));

        // Las posibles conclusiones
        this.resiste = new FuzzyValue(conclusion, "resiste");
        this.mantenimiento = new FuzzyValue(conclusion, "mantenimiento");
        this.no_resiste = new FuzzyValue(conclusion, "no_resiste");




        //definimos los valores que vamos a usar para definir las reglas

        this.peso_poco = new FuzzyValue(conjuntos.getPeso(), "poco");
        this.peso_mediano = new FuzzyValue(conjuntos.getPeso(), "mediano");
        this.peso_mucho = new FuzzyValue(conjuntos.getPeso(), "mucho");
        this.peso_demaciado = new FuzzyValue(conjuntos.getPeso(), "demaciado");


        this.cercano_mar = new FuzzyValue(conjuntos.getCercaniaMar(), "cercano");
        this.urbano = new FuzzyValue(conjuntos.getCercaniaMar(), "urbano");
        this.rural = new FuzzyValue(conjuntos.getCercaniaMar(), "rural");


        this.historia_poca = new FuzzyValue(conjuntos.getHistoriaCarga(), "poco_cargada");
        this.historia_mediana = new FuzzyValue(conjuntos.getHistoriaCarga(), "cargada_medianamente");
        this.historia_bastante = new FuzzyValue(conjuntos.getHistoriaCarga(), "muy_cargada");
        this.historia_demaciada = new FuzzyValue(conjuntos.getHistoriaCarga(), "demaciado_cargada");
        


        this.fatiga_bajo = new FuzzyValue(conjuntos.ciclosFatiga(), "ciclos_bajos");
        this.fatiga_medio = new FuzzyValue(conjuntos.ciclosFatiga(), "ciclos_medios");
        this.fatiga_mucho = new FuzzyValue(conjuntos.ciclosFatiga(), "muchos_ciclos");
        this.fatiga_demaciado = new FuzzyValue(conjuntos.ciclosFatiga(), "demaciados_ciclos");


        this.peso_normal = this.peso_poco.fuzzyUnion(this.peso_mediano);
        this.no_mar = this.urbano.fuzzyUnion(this.rural);
        this.historia_normal = this.historia_poca.fuzzyUnion(this.historia_mediana);
        this.fatiga_normal = this.fatiga_bajo.fuzzyUnion(this.fatiga_medio);




        //primera regla
        // Si peso uniforme mediano y cercano al mar e historia de carga promedio mediana y ciclos de fatiga bajos. RESISTE
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mediano);
        regla.addAntecedent(cercano_mar);
        regla.addAntecedent(historia_mediana);
        regla.addAntecedent(fatiga_bajo);
        regla.addConclusion(resiste);
        this.reglas.add(regla);


        //2. Si mucho peso uniforme y urbano y poca historia de carga promedio y ciclos de fatiga bajos. RESISTE
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mucho);
        regla.addAntecedent(urbano);
        regla.addAntecedent(historia_poca);
        regla.addAntecedent(fatiga_bajo);
        regla.addConclusion(resiste);
        this.reglas.add(regla);


        //3. Si poco pes uniforme y RURAL y demasiada historia de carga promedio y ciclos de fatiga promedio. MANTENIMIENTO
        regla = new FuzzyRule();
        regla.addAntecedent(peso_poco);
        regla.addAntecedent(rural);
        regla.addAntecedent(historia_demaciada);
        regla.addAntecedent(fatiga_medio);
        regla.addConclusion(mantenimiento);
        this.reglas.add(regla);


        //4. Si peso uniforme mediano y urbano e historia de carga promedio mediana y ciclos de fatiga medianos.MANTENIMIENTO
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mediano);
        regla.addAntecedent(urbano);
        regla.addAntecedent(historia_mediana);
        regla.addAntecedent(fatiga_medio);
        regla.addConclusion(mantenimiento);
        this.reglas.add(regla);


        // 5. Si demasiado peso uniforme y campo y poca historia de carga promedio y demasiados ciclos de fatiga. NO RESISTE
        regla = new FuzzyRule();
        regla.addAntecedent(peso_demaciado);
        regla.addAntecedent(rural);
        regla.addAntecedent(historia_poca);
        regla.addAntecedent(fatiga_demaciado);
        regla.addConclusion(no_resiste);
        this.reglas.add(regla);


        // 6. Si mucho peso uniforme y cercano del mar y poca historia de carga promedio y muchos ciclos de fatiga. NO RESISTE
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mucho);
        regla.addAntecedent(cercano_mar);
        regla.addAntecedent(historia_poca);
        regla.addAntecedent(fatiga_mucho);
        regla.addConclusion(no_resiste);
        this.reglas.add(regla);


        // 7. Si poco peso uniforme y campo y poca historia de carga promedio y pocos ciclos de fatiga. RESISTE
        regla = new FuzzyRule();
        regla.addAntecedent(peso_poco);
        regla.addAntecedent(rural);
        regla.addAntecedent(historia_poca);
        regla.addAntecedent(fatiga_bajo);
        regla.addConclusion(resiste);
        this.reglas.add(regla);


        //8. Si demasiado peso uniforme y campo e historia de carga promedio mediana y demasiados ciclos de fatiga. NO RESISTE
        regla = new FuzzyRule();
        regla.addAntecedent(peso_demaciado);
        regla.addAntecedent(rural);
        regla.addAntecedent(historia_mediana);
        regla.addAntecedent(fatiga_demaciado);
        regla.addConclusion(no_resiste);
        this.reglas.add(regla);


        //9. Si peso uniforme mediano y cerca del mar y poca historia de carga promedio y demasiados ciclos de fatiga. NO RESISTE
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mediano);
        regla.addAntecedent(cercano_mar);
        regla.addAntecedent(historia_mediana);
        regla.addAntecedent(fatiga_demaciado);
        regla.addConclusion(no_resiste);
        this.reglas.add(regla);

        // 10. Si mucho peso uniforme y cerca del mar y poca historia de carga promedio y ciclos de fatiga medianos. MANTENIMIENTO
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mucho);
        regla.addAntecedent(cercano_mar);
        regla.addAntecedent(historia_poca);
        regla.addAntecedent(fatiga_medio);
        regla.addConclusion(no_resiste);
        this.reglas.add(regla);

        // 11. Si hay demaciado peso la viga no resiste
        regla = new FuzzyRule();
        regla.addAntecedent(peso_demaciado);
        regla.addConclusion(no_resiste);
        this.reglas.add(regla);


        // 12. Si el peso es mucho y esta lejos del mar y los ciclos de fatiga son bajos y la historia de carga baja
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mucho);
        regla.addAntecedent(rural);
        regla.addAntecedent(historia_poca);
        regla.addAntecedent(fatiga_bajo);
        regla.addConclusion(resiste);
        this.reglas.add(regla);

        // 13. Si el peso es poco y esta cerca del mar y los ciclos de fatiga son bajos y la historia de carga baja
        regla = new FuzzyRule();
        regla.addAntecedent(peso_poco);
        regla.addAntecedent(cercano_mar);
        regla.addAntecedent(historia_poca);
        regla.addAntecedent(fatiga_bajo);
        regla.addConclusion(mantenimiento);
        this.reglas.add(regla);


        // 14. Historia demaciada
        regla = new FuzzyRule();
        regla.addAntecedent(historia_demaciada);
        regla.addConclusion(no_resiste);
        this.reglas.add(regla);

        // 15. Todo normal
        regla = new FuzzyRule();
        regla.addAntecedent(peso_normal);
        regla.addAntecedent(no_mar);
        regla.addAntecedent(historia_normal);
        regla.addAntecedent(fatiga_normal);
        regla.addConclusion(resiste);
        this.reglas.add(regla);


        //16 reglas para las variables que no son muy grandes pero que igual es
        //mucho y por lo tengo se requiere de mantenimiento
        regla = new FuzzyRule();
        regla.addAntecedent(peso_mucho);
        regla.addConclusion(mantenimiento);
        this.reglas.add(regla);

        regla = new FuzzyRule();
        regla.addAntecedent(historia_bastante);
        regla.addConclusion(mantenimiento);
        this.reglas.add(regla);

        regla = new FuzzyRule();
        regla.addAntecedent(fatiga_mucho);
        regla.addConclusion(mantenimiento);
        this.reglas.add(regla);



    }


    /**Ejecuta las reglas en base a los valores dados y devuelve un string
     * indicando a que conclusion se llego
     *
     * @param peso El peso uniforme que se le va a poner a la viga
     * @param millas_mar A que distancia (en millas) se encuentra del mar
     * @param historia_carga La edad (en anos) que tiene la viga
     * @param fatiga Los ciclos a los que ha sido expuesta la viga
     *
     * @return Una cadena que indica si resiste (resiste), si se requiere
     * mantenimiento (mantenimiento) o si la viga no va a resistir en las
     * condiciones dadas (no_resiste */
    public String run(int peso, int millas_mar, int historia_carga, int fatiga) throws FuzzyException
    {

        //obtenemos un valor al ejecutar las reglas
        FuzzyValue val = this.getValue(peso, millas_mar, historia_carga, fatiga);

        //ocurrio un error
        if(val == null)
        {
            return null;
        }


        //desfusificamos el valor y obtenemos un porcentaje que nos indica si la
        //viga resiste o no
        double valor = val.momentDefuzzify();



        if(valor < 25)
        {
            return "resiste";
        }else if(valor >= 25 && valor < 75){
            return "mantenimiento";
        }else if(valor >= 75){
            return "no_resiste";
        }else{
            return "VALOR NO VALIDO";
        }
    }


    /**Ejecuta la regla difusa y devuelve un valor difuso a partir de la reglas
     *
     * @param peso El peso uniforme que se le va a poner a la viga
     * @param millas_mar A que distancia (en millas) se encuentra del mar
     * @param historia_carga La edad (en anos) que tiene la viga
     * @param fatiga Los ciclos a los que ha sido expuesta la viga
     *
     * @return Un valor difuso que puede ser utilizado para encontrar la
     * respuesta al problema
     * */
    private FuzzyValue getValue(int peso, int millas_mar, int historia_carga, int fatiga) throws FuzzyException
    {
        FuzzyValue respuesta = null;


        // vemos si podemos ejecutar todas las reglas
        for(FuzzyRule rule: this.reglas)
        {
            rule.removeAllInputs();

            FuzzyValue input1 = this.generarInput(this.conjuntos.getPeso(), peso, 100, 4000);
            FuzzyValue input2 = this.generarInput(this.conjuntos.getCercaniaMar(), millas_mar, 5, 80);
            FuzzyValue input3 = this.generarInput(this.conjuntos.getHistoriaCarga(), historia_carga, 10, 150);
            FuzzyValue input4 = this.generarInput(this.conjuntos.ciclosFatiga(), fatiga, 500000, 10000000);




            // agregamos los inputs que corresponden
            for(int i = 0; i < rule.antecedentsSize(); i++)
            {
                FuzzyValue val = rule.antecedentAt(i);



                if((val.getLinguisticExpression().contains("poco") || val.getLinguisticExpression().contains("mucho") || val.getLinguisticExpression().contains("mediano") || val.getLinguisticExpression().contains("demaciado")) && !(val.getLinguisticExpression().contains("poco_cargada") || val.getLinguisticExpression().contains("muchos_ciclos") || val.getLinguisticExpression().contains("demaciado_cargada") || val.getLinguisticExpression().contains("demaciados_ciclos")))
                {
                    System.out.println("Input 1");
                    rule.addInput(input1);
                }else if(val.getLinguisticExpression().contains("cercano") || val.getLinguisticExpression().contains("urbano") || val.getLinguisticExpression().contains("rural")){
                    System.out.println("Input 2");
                    rule.addInput(input2);
                }else if(val.getLinguisticExpression().contains("poco_cargada") || val.getLinguisticExpression().contains("cargada_medianamente") || val.getLinguisticExpression().contains("demaciado_cargada") || val.getLinguisticExpression().contains("muy_cargada")){
                    System.out.println("Input 3");
                    rule.addInput(input3);
                }else if(val.getLinguisticExpression().contains("ciclos_bajos") || val.getLinguisticExpression().contains("ciclos_medios") || val.getLinguisticExpression().contains("muchos_ciclos") || val.getLinguisticExpression().contains("demaciados_ciclos")){
                    System.out.println("Input 4");
                    rule.addInput(input4);
                }

            }




            //si los inputs hacen match, entonces ejecutamos la regla y
            //obtenemos le primer valor difuso ya que tenemos solo una
            //conclusion en las reglas
            if(rule.testRuleMatching())
            {
                FuzzyValue val = rule.execute().fuzzyValueAt(0);

                if(respuesta == null)
                {
                    respuesta = val;
                }else{
                    //si ya teniamos una respuesta debemos unir con los valores
                    //aneteriores
                    respuesta.fuzzyUnion(val);
                }

            }
            
        }

        return respuesta;
    }


    /**Genera una FuzzyValue en base a input dado fijandose de que el valor
     * respete los rangos [0, max]
     *
     * @param variable El dominio en que se quiere poner el input
     * @param val El valor del input
     * @param rango El rango que puede tener este input
     * @param max El maximo valor que puede tener el input
     *
     * @return Una FuzzyValue para el input*/
    private FuzzyValue generarInput(FuzzyVariable variable, int val, int rango, int max)throws FuzzyException
    {
        if(val - rango < 0)
        {
            return new FuzzyValue(variable, new TriangleFuzzySet(val, val, val+rango));
        }else if(val + rango > max){
            return new FuzzyValue(variable, new TriangleFuzzySet(val - rango, val, val));
        }else{
            return new FuzzyValue(variable, new TriangleFuzzySet(val - rango, val, val + rango));
        }
    }


    /**A partir de los inputs dados retorna un string explicando la conclusion.
     *
     * @param peso El peso uniforme que se le va a poner a la viga
     * @param millas_mar A que distancia (en millas) se encuentra del mar
     * @param historia_carga La edad (en anos) que tiene la viga
     * @param fatiga Los ciclos a los que ha sido expuesta la viga
     *
     * @return Un string explicando la conclusion
     * */
    public String obtenerExplicacion(int peso, int millas_mar, int historia_carga, int fatiga) throws FuzzyException
    {
        String val = this.run(peso, millas_mar, historia_carga, fatiga);

        if(val == null)
        {
            return "No hay regla definida";
        }

        if(val.equals("resiste"))
        {
            return "La viga resiste en las condiciones dadas";
        }else if(val.equals("mantenimiento"))
        {
            return "La viga requiere de mantenimiento para poder seguir funcionando";
        }else if(val.equals("no_resiste")){
            return "La viga no resiste las condiciones dadas";
        }else{
            return "Variable linguistica no valida: " + val;
        }

    }

}
