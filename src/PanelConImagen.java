
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;


public class PanelConImagen extends javax.swing.JPanel {

    private ImageIcon imagen; 
    
    //Constructor 
    public PanelConImagen(String nombre) {
        
        //iniciamos los elementos dispuestos en el panel
        initComponents();
        
        //si el archivo no es nulo
        if(nombre!=null){
         
            //creamos un objeto imagen, para el panel
            imagen = new ImageIcon(nombre);
            
        }
        //si no, entonces la imagen la dejamos en nulo
        else{
        
            imagen = null;
        
        }
    }

    @Override
    
    public void paint(Graphics g){ //reescribimos el metodo paint
        
        //si la imagen fue creada
        if(imagen != null){
        
            //se dibuja la imagen en todo el panel (la imagen guardada en el atributo imagen)
            g.drawImage(imagen.getImage(), 0, 0, getWidth(), getHeight(), this);
            setOpaque(false);   //el panel no usa su fondo por defecto 
        } 
        
        //en caso de que no se haya creado la imagen
        else {
            
            setOpaque(true); //dibuja su fondo por defecto
        }
 
        super.paint(g); // hacemos que el panel siga dibujando los otros componentes    
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
