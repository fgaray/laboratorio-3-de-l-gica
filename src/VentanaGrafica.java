
import java.awt.BorderLayout;
import nrc.fuzzy.*;


/**Clase autogenerada por netbeans menos la funcion
 * botonDiagnosticoActionPerformed donde se realiza la inferencia logica*/
public class VentanaGrafica extends javax.swing.JFrame {

    //atributo de clase PanelConImagen, creamos y cargamos su imagen
    private final PanelConImagen contenedor = new PanelConImagen("fondo_lab.jpg");
    
    //constructor
    public VentanaGrafica() {
        
        //ingresamos primero el panel con imagen 
        setContentPane(contenedor);
        //inicializamos los componentes que tendra la interfaz sobre el frame
        initComponents();
       
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        textPeso = new javax.swing.JLabel();
        textDisMar = new javax.swing.JLabel();
        textFatiga = new javax.swing.JLabel();
        textHisto = new javax.swing.JLabel();
        barraPeso = new javax.swing.JSlider();
        barraDisMar = new javax.swing.JSlider();
        barraFatiga = new javax.swing.JSlider();
        barraHisto = new javax.swing.JSlider();
        botonDiagnostico = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        mostrarInferencia = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Estado de Vigas de Acero");
        setResizable(false);

        textPeso.setBackground(new java.awt.Color(254, 254, 254));
        textPeso.setFont(new java.awt.Font("Ubuntu", 1, 16));
        textPeso.setForeground(new java.awt.Color(1, 1, 1));
        textPeso.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textPeso.setText("  Peso (Kg)  ");
        textPeso.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));

        textDisMar.setFont(new java.awt.Font("Ubuntu", 1, 16));
        textDisMar.setForeground(new java.awt.Color(1, 1, 1));
        textDisMar.setText("  Distancia del Mar (Millas)");
        textDisMar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));

        textFatiga.setFont(new java.awt.Font("Ubuntu", 1, 16));
        textFatiga.setForeground(new java.awt.Color(1, 1, 1));
        textFatiga.setText("  Fatiga (Ciclos)");
        textFatiga.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));
        textFatiga.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        textHisto.setFont(new java.awt.Font("Ubuntu", 1, 16));
        textHisto.setForeground(new java.awt.Color(1, 1, 1));
        textHisto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        textHisto.setText("  Historia de Carga (Años)");
        textHisto.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));

        barraPeso.setBackground(new java.awt.Color(198, 196, 227));
        barraPeso.setForeground(new java.awt.Color(1, 1, 1));
        barraPeso.setMajorTickSpacing(500);
        barraPeso.setMaximum(4000);
        barraPeso.setMinorTickSpacing(100);
        barraPeso.setOrientation(javax.swing.JSlider.VERTICAL);
        barraPeso.setPaintLabels(true);
        barraPeso.setPaintTicks(true);
        barraPeso.setValue(0);
        barraPeso.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));

        barraDisMar.setBackground(new java.awt.Color(199, 196, 239));
        barraDisMar.setForeground(new java.awt.Color(1, 1, 1));
        barraDisMar.setMajorTickSpacing(10);
        barraDisMar.setMaximum(80);
        barraDisMar.setMinorTickSpacing(2);
        barraDisMar.setOrientation(javax.swing.JSlider.VERTICAL);
        barraDisMar.setPaintLabels(true);
        barraDisMar.setPaintTicks(true);
        barraDisMar.setValue(0);
        barraDisMar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));

        barraFatiga.setBackground(new java.awt.Color(199, 196, 236));
        barraFatiga.setForeground(new java.awt.Color(1, 1, 1));
        barraFatiga.setMajorTickSpacing(1000000);
        barraFatiga.setMaximum(10000000);
        barraFatiga.setMinorTickSpacing(200000);
        barraFatiga.setOrientation(javax.swing.JSlider.VERTICAL);
        barraFatiga.setPaintLabels(true);
        barraFatiga.setPaintTicks(true);
        barraFatiga.setValue(0);
        barraFatiga.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));

        barraHisto.setBackground(new java.awt.Color(204, 200, 238));
        barraHisto.setForeground(new java.awt.Color(1, 1, 1));
        barraHisto.setMajorTickSpacing(25);
        barraHisto.setMaximum(150);
        barraHisto.setMinorTickSpacing(5);
        barraHisto.setOrientation(javax.swing.JSlider.VERTICAL);
        barraHisto.setPaintLabels(true);
        barraHisto.setPaintTicks(true);
        barraHisto.setValue(0);
        barraHisto.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, null, java.awt.Color.gray, java.awt.Color.gray));
        barraHisto.setRequestFocusEnabled(false);

        botonDiagnostico.setText("Diagnostico");
        botonDiagnostico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //Aca probamos el input con las reglas difusas
                botonDiagnosticoActionPerformed(evt);
            }
        });

        mostrarInferencia.setColumns(20);
        mostrarInferencia.setRows(5);
        jScrollPane1.setViewportView(mostrarInferencia);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(barraPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(91, 91, 91)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(textDisMar, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(69, 69, 69))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(barraDisMar, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(130, 130, 130)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(barraFatiga, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(textFatiga, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE))
                        .addGap(67, 67, 67)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textHisto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(barraHisto, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(71, 71, 71))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(456, 456, 456)
                        .addComponent(botonDiagnostico, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(276, 276, 276)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 525, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(textFatiga, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                        .addComponent(textHisto, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
                        .addComponent(textDisMar, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
                    .addComponent(textPeso, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
                .addGap(75, 75, 75)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(barraFatiga, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barraHisto, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barraDisMar, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barraPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addComponent(botonDiagnostico, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(77, 77, 77))
        );

        pack();
    }// </editor-fold>


/**Metodo que ejecuta la accion del boton Diagnostico.
 *
 * Muestra el resultado de la regla en el cuadro de texto "mostrarInferencia"
 * */
private void botonDiagnosticoActionPerformed(java.awt.event.ActionEvent evt) {                                                 

    //obtenemos los valores de las barras
    int peso = barraPeso.getValue();
    int dist_mar = barraDisMar.getValue();
    int hist_carga = barraHisto.getValue();
    int fatiga = barraFatiga.getValue();

    try{

        RegasDifusas reglas;
        reglas = new RegasDifusas(new ConjuntosDifusos());
        mostrarInferencia.setText(reglas.obtenerExplicacion(peso, dist_mar, hist_carga, fatiga));


    }catch(FuzzyException e){
        //ocurrio un error en la libreria de logica difusa
        mostrarInferencia.setText("Error en la libreria de lógica difusa");
        System.out.println(e);
    }


    

}//GEN-LAST:event_botonDiagnosticoActionPerformed


                                              


   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaGrafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new VentanaGrafica().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify
    private javax.swing.JSlider barraDisMar;
    private javax.swing.JSlider barraFatiga;
    private javax.swing.JSlider barraHisto;
    private javax.swing.JSlider barraPeso;
    private javax.swing.JButton botonDiagnostico;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea mostrarInferencia;
    private javax.swing.JLabel textDisMar;
    private javax.swing.JLabel textFatiga;
    private javax.swing.JLabel textHisto;
    private javax.swing.JLabel textPeso;
    // End of variables declaration
}

