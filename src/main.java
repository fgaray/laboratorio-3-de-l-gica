import nrc.fuzzy.*;


/**Entrada del programa*/
public class main 
{
    public static void main (String [] args)throws FuzzyException
    {
        VentanaGrafica v = new VentanaGrafica();
        v.setVisible(true);
    }

    /**Realiza pruebas de rango sobre las reglas difusas
     *
     * @param reglas Las reglas difusas a probar 
     * */
    static void test(RegasDifusas reglas)
    {

        int var = 0;

        //probamos que el sistema nos entrege un resultado en los rangos de
        //las variables
        for(int peso = 2; peso < 4000; peso += 100)
        {
            for(int millas_mar = 0; millas_mar < 80; millas_mar += 5)
            {
                for(int historia_carga = 140; historia_carga < 150; historia_carga += 10)
                {
                    for(int fatiga = 100; fatiga < 10000000; fatiga = fatiga + 1000)
                    {

                        if(var % 100000 == 0)
                        {
                            System.out.println("Peso: " + peso);
                            System.out.println("Millas: " + millas_mar);
                            System.out.println("Historia Carga: " + historia_carga);
                            System.out.println("Fatiga: " + fatiga);
                        }


                        try{

                            reglas.run(1000, 10, 10, 10).toString();
                        }catch(FuzzyException e){
                            System.out.println("");
                            System.out.println("Error!");
                            System.out.println("Peso: " + peso);
                            System.out.println("Millas: " + millas_mar);
                            System.out.println("Historia Carga: " + historia_carga);
                            System.out.println("Fatiga: " + fatiga);
                            return;
                        }

                        var++;

                    }
                }
            }
        }
    }
}
